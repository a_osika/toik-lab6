package com.company;

import java.util.Random;

class QuizImpl implements Quiz {

    private final int guessedValue;

    public QuizImpl() {
        Random generate = new Random();
        this.guessedValue = generate.nextInt((MAX_VALUE - MIN_VALUE) + 1) + MIN_VALUE;
    }

    public QuizImpl(int value) {
        this.guessedValue = value;
    }

    @Override
    public void isCorrectValue(int value) throws ParamTooLarge, ParamTooSmall {
        if (this.guessedValue > value) {
            throw new ParamTooSmall();
        } else if (this.guessedValue < value) {
            throw new ParamTooLarge();
        }
    }
}